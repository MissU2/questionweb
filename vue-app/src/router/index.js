import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
    mode: 'history', //去掉#号  mode有两种模式 'history'（没有# 需要Nginx配置） 'hash'（有#号）
    routes: [
        {
            path: '/vue/',
            name: 'test',
            component: resolve => require(['../page/index.vue'], resolve)
        },

    ]
})

export default router