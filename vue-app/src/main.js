import Vue from "vue";
import App from "./App.vue";
import Cloudbase from "@cloudbase/vue-provider";
import config from "../cloudbaserc";
import Vant from 'vant';
import 'vant/lib/index.css';
import VueRouter from 'vue-router'
import router from './router/index.js';

Vue.use(Vant);
Vue.use(VueRouter);

Vue.config.productionTip = false;

Vue.use(Cloudbase, {
  env: process.env.VUE_APP_ENV_ID || config.envId,
});

new Vue({
  render: (h) => h(App),
  router
}).$mount("#app");
